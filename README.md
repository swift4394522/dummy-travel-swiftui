<h1 align="center">
<br>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/swift/swift-original.svg" width="80px" />       
<br>
<br>
Dummy Travel App - SwiftUI
</h1>

<h4 align="center">Travel SwiftUI with UIKit Web View, Sorting and Filtering</h4>
<p align="center">. Content View as entry point which contains an array of models for displaying the cards</p>
<p align="center">. Binded search term and filter type for handling search/filtering</p>
<p align="center">. Geometry reader gets the width/height of contains so we can set a background image correctly to fill the container with the image</p>

<div align="center">
   <img align="center" src="./TravelApp/travel.gif" width="230px">
</div>
