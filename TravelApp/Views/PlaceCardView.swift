//
//  PlaceCardView.swift
//  TravelApp
//
//  Created by Aidan Walker on 08/06/2023.
//

import Foundation
import SwiftUI

struct PlaceCard: View {
    var model: PlaceModel

    var body: some View {
        VStack(alignment: .leading) {
            // Image
            Image(model.location.imageName)
                .resizable()
                .aspectRatio(contentMode: .fill)

            // Title
            Text(model.title)
                .bold()
                .font(.system(size: 24))
                .padding(.bottom, 5)

            // Description
            Text(model.description)
                .foregroundColor(Color(.secondaryLabel))
                .padding(.bottom, 10)

            // Location title
            Text(model.location.title)
                .foregroundColor(Color(.secondaryLabel))

            // Type
            Text(model.type.rawValue.lowercased())
                .foregroundColor(Color(.secondaryLabel))
        }
        .padding()
        .background(Color.white)
    }
}

