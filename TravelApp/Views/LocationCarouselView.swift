//
//  LocationCarouselView.swift
//  TravelApp
//
//  Created by Aidan Walker on 07/06/2023.
//

import Foundation
import SwiftUI

struct LocationCarouselView: View {
    let locations: [LocationModel]
    
    // instantiate 4 location objects
    init() {
        self.locations = [
            LocationModel(title: "New York City, USA", imageName: "location1"),
            LocationModel(title: "London, UK", imageName: "location2"),
            LocationModel(title: "Tokyo, Japan", imageName: "location3"),
            LocationModel(title: "San Francisco, USA", imageName: "location4")
        ]
    }
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(locations) { model in
                    NavigationLink(
                        destination: LocationDetailView(title: model.title),
                        label: {
                            LocationCard(model: model)
                                .frame(width: 300, height: 170, alignment: .center)
                                .cornerRadius(12)
                                .padding()
                                .padding(.trailing, -25)
                        })
                }
            }
        }
    }
}

struct LocationDetailView: View {
    var title: String

    var body: some View {
        ScrollView {
            VStack {
                
            }
        }
        .navigationTitle(title)
    }
}


struct LocationCard: View {
    let model: LocationModel
    
    var body: some View {
        ZStack {
            // GeometryReader - Special type of View. Allows us to access the width/height of container
            // helps to fill each card correctly with the bg image
            GeometryReader { proxy in
                Image(model.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: proxy.size.width, height: proxy.size.height, alignment: .center)
            }
            
            VStack(alignment: .leading) {
                Spacer()
                HStack {
                    Text(model.title)
                        .bold()
                        .foregroundColor(Color.white)
                        .font(.system(size: 32))
                        .padding(.leading, 4)
                    Spacer()
                }
                
            }
        }
    }
}
