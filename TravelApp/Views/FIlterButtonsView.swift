//
//  FIlterButtonsView.swift
//  TravelApp
//
//  Created by Aidan Walker on 08/06/2023.
//

import Foundation
import SwiftUI

struct FilterButtonsView: View {
    @Binding var filterType: PlaceType?
    
    // type Binding of PlaceType optional
    // whenever there is a button type, assign the state to the respective type
    init(type: Binding<PlaceType?>) {
        _filterType = type
    }
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(PlaceType.allCases, id: \.self) { type in
                    let name = type.rawValue.uppercased() // get capitalised version
                    Button(action: {
                        filterType = type
                    }, label: {
                        Text(name)
                            .bold()
                            .frame(width: 150, height: 50, alignment: .center)
                            .background(Color.white)
                            .cornerRadius(10)
                    })
                        .padding(.leading, 5)
                }
            }
        }
    }
}
