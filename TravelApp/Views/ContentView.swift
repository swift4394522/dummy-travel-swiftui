//
//  ContentView.swift
//  TravelApp
//
//  Created by Aidan Walker on 07/06/2023.
//

import SwiftUI

struct ContentView: View {
    @State var searchTerm = ""
    // determines what web view to show when card is tapped
    @State var filterType: PlaceType? // by default if we dont have a filter, show everything
    
    // array of type PlaceModel which we use to loop over to display the cards
    @State var models: [PlaceModel] = [
        PlaceModel(type: .mall,
              location: LocationModel(title: "New York",
                                 imageName: "location1"),
              title: "New York City Macys Mall",
              description: "Huge mall with tons of things to do for the whole family."),
        PlaceModel(type: .restaurant,
              location: LocationModel(title: "Japan",
                                 imageName: "location3"),
              title: "Japanese Food Frenzy",
              description: "Great japanese food to go for everyone."),
        PlaceModel(type: .airport,
              location: LocationModel(title: "London",
                                 imageName: "location2"),
              title: "London City Airport",
              description: "Neat and clean airport that services city of London"),
        PlaceModel(type: .sportingVenue,
              location: LocationModel(title: "San Francisco",
                                 imageName: "location4"),
              title: "49ers Stadium",
              description: "Beautiful football field for home games."),
        PlaceModel(type: .unknown,
              location: LocationModel(title: "New York",
                                 imageName: "location1"),
              title: "Central Park",
              description: "Enormous park in the middle of the city; great for family visits.")
    ]
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.green
                    .edgesIgnoringSafeArea(.all)
                
                ScrollView {
                    VStack {
                        TextField("Search", text: $searchTerm)
                            .padding(12)
                            .background(Color.white)
                            .cornerRadius(14)
                            .padding()
                        
                        LocationCarouselView()
                          //  .background(Color.blue)
                        FilterButtonsView(type: $filterType)
                        
                        VStack {
                            ForEach(models) { place in
                                // determine what card to show via the filtering/search if theres no search term
                                if searchTerm.isEmpty {
                                    if filterType == nil {
                                        NavigationLink(
                                            destination: WebView(string: "https://www.google.com/search?q=new+york+city"),
                                            label: {
                                                PlaceCard(model: place)
                                                    .cornerRadius(12)
                                                    .padding()
                                            }
                                        )
                                    }
                                    else if let filterType = self.filterType, filterType == place.type {

                                        NavigationLink(
                                            destination: WebView(string: "https://www.google.com/search?q=new+york+city"),
                                            label: {
                                                PlaceCard(model: place)
                                                    .cornerRadius(12)
                                                    .padding()
                                            }
                                        )
                                    }
                                }
                                // determines what type to show for searching or searching and filter type
                                else {
                                    if place.title.lowercased().hasPrefix(searchTerm.lowercased()) {
                                        if filterType == nil {
                                            NavigationLink(
                                                destination: WebView(string: "https://www.google.com/search?q=new+york+city"),
                                                label: {
                                                    PlaceCard(model: place)
                                                        .cornerRadius(12)
                                                        .padding()
                                                }
                                            )
                                        }
                                        else if let filterType = self.filterType, filterType == place.type {
                                            NavigationLink(
                                                destination: WebView(string: "https://www.google.com/search?q=new+york+city"),
                                                label: {
                                                    PlaceCard(model: place)
                                                        .cornerRadius(12)
                                                        .padding()
                                                }
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            .navigationTitle("Travel")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
