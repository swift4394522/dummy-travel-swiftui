//
//  TravelAppApp.swift
//  TravelApp
//
//  Created by Aidan Walker on 07/06/2023.
//

import SwiftUI

@main
struct TravelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
