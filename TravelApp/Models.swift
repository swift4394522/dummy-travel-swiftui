//
//  Models.swift
//  TravelApp
//
//  Created by Aidan Walker on 07/06/2023.
//

import SwiftUI

// Location - made Identifiable as we have an id which we can use when looping
class LocationModel: ObservableObject, Identifiable {
    var id = UUID()
    var title: String = ""
    var imageName: String = ""
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}

// Type of Place
// Inherits from string
enum PlaceType: String, CaseIterable {
    case restaurant
    case mall
    case sportingVenue
    case airport
    case unknown
}

// Place
class PlaceModel: ObservableObject, Identifiable {
    var id = UUID()
    var type: PlaceType = .unknown
    var location = LocationModel(title: "", imageName: "")
    var title: String = ""
    var description: String = ""
    
    init(type: PlaceType,
         location: LocationModel,
         title: String,
         description: String) {
        self.type = type
        self.title = title
        self.description = description
        self.location = location
    }
}

